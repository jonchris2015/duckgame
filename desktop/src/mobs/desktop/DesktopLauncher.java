package mobs.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import controllers.GameplayController;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 720;
		config.height = 480;
		config.addIcon("images/Game Icon.png", Files.FileType.Local);
		config.resizable = false;
		config.title = "Duck Adventures";
		new LwjglApplication(new GameplayController(), config);
	}
}
