package huds;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

import helper.GameInfo;
import mobs.Duck;

/**
 * Created by C on 1/13/2017.
 */

public class HealthBar {
    private Rectangle rectangle;
    private ShapeRenderer sr;
    private ShapeRenderer outline;
    private float width;

    public HealthBar(){
        rectangle = new Rectangle();
        rectangle.setY(GameInfo.HEIGHT*(9/10f));
        outline = new ShapeRenderer();

        fullHealthBar();
    }

    private void fullHealthBar() {
    }

    public void setOutlineWidth(float width){
        this.width = width;
    }

    public void renderHealthBar(ShapeRenderer sr){
        this.sr = sr;
        sr.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public void reduceWidth(float x){
        rectangle.width = rectangle.width + x;
    }

    public void Update(Duck duck){
        rectangle.setX(((duck.getX()) + duck.getWidth()/2) - GameInfo.WIDTH/2f);
    }

    public void renderOutline(ShapeRenderer sr){
        sr.rect(rectangle.x, rectangle.y, width, rectangle.height);
    }

    public void setX(float x) {
        rectangle.x = x;
    }

    public void setSize(float x, float y){
        rectangle.setSize(x, y);
    }
}
