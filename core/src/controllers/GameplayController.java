package controllers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import screens.EndLevel;
import screens.Gameplay;
import screens.HelpScreen;
import screens.LoseScreen;
import screens.MainMenu;
import singleton.TextureSingleton;

public class GameplayController extends Game {
    private Screen screen;
    private SpriteBatch batch;
    private HashMap<String, Texture> textures;

    @Override
    public void create () {
        textures = TextureSingleton.getSingleton();
        batch = new SpriteBatch();
        mainMenu();
//        startGame();
    }

    public void mainMenu() {
        screen = new MainMenu(this, batch);
        setScreen(screen);
        Gdx.input.setInputProcessor(((MainMenu)screen).getStage());
    }

    public void mainMenuContinue(float x) {
        screen = new MainMenu(this, batch);
        ((MainMenu)screen).setCurrentX(x);
        setScreen(screen);
        Gdx.input.setInputProcessor(((MainMenu)screen).getStage());
    }

    public void startGame() {
        screen = new Gameplay(this, batch);
        setScreen(screen);
        Gdx.input.setInputProcessor(((InputProcessor)screen));
        Controllers.addListener((ControllerListener) screen);
    }

    public void help(float x){
        screen = new HelpScreen(this, batch);
        ((HelpScreen)screen).setX(x);
        setScreen(screen);
        Gdx.input.setInputProcessor(((HelpScreen)screen).getStage());
    }

    public void endOfLevel(float x) {
        screen = new EndLevel(this, batch);
        ((EndLevel)screen).setX(x);
        setScreen(screen);
        Gdx.input.setInputProcessor(((EndLevel)screen).getStage());
    }

    public void lose(float x) {
        screen = new LoseScreen(this, batch);
        ((LoseScreen)screen).setDuckDrawing(x);
        setScreen(screen);
        Gdx.input.setInputProcessor(((LoseScreen)screen).getStage());
    }

    public HashMap<String, Texture> getTextures(){
        return textures;
    }

    public void dispose(){
        screen.dispose();
    }

    @Override
    public void render () {
        super.render();
    }
}
