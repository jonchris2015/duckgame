package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.HashMap;

import controllers.GameplayController;
import helper.GameInfo;

/**
 * Created by C on 1/2/2017.
 */

public class EndLevel implements MasterScreen, Screen, InputProcessor{
    private Stage stage;
    private SpriteBatch batch;
    private GameplayController controller;
    private ImageButton replayLevel;
    private ImageButton mainMenu;
    private ImageButton nextLevel;
    private static final int WIDTH = 720;
    private static final int HEIGHT = 480;
    private HashMap<String, Texture> textures;
    private float x = WIDTH/2;
    private Sprite sprite;


    public EndLevel(GameplayController controller, SpriteBatch batch){
        stage = new Stage();
        this.batch = batch;
        this.controller = controller;
        textures = controller.getTextures();

        sprite = new Sprite(textures.get("DuckWin.png"));

        replayLevel = new ImageButton(new SpriteDrawable(new Sprite(textures.get("Replay.png"))));
        mainMenu = new ImageButton(new SpriteDrawable(new Sprite(textures.get("MainMenu.png"))));
        nextLevel = new ImageButton(new SpriteDrawable(new Sprite(textures.get("Next.png"))));

        positionButtons();
        addListener();

        stage.addActor(replayLevel);
        stage.addActor(mainMenu);
        stage.addActor(nextLevel);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void positionButtons() {
        replayLevel.setPosition(WIDTH/2 - (mainMenu.getWidth()/2 + mainMenu.getWidth() + 3), HEIGHT/4);
        mainMenu.setPosition(WIDTH/2 - mainMenu.getWidth()/2, HEIGHT/4);
        nextLevel.setPosition(WIDTH/2 + (mainMenu.getWidth()/2 + 3), HEIGHT/4);
    }

    @Override
    public void addListener() {
        replayLevel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.startGame();
            }
        });
        mainMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.mainMenuContinue(x);
            }
        });
        nextLevel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Implement later");
            }
        });

    }

    public Stage getStage(){
        return stage;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glClearColor(.6f, .8f, 1f, 0f);

        stage.draw();

        batch.begin();
        sprite.draw(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        stage.dispose();
        sprite.getTexture().dispose();
    }

    public void setX(float x) {
        this.x = x;
        sprite.setPosition(x - sprite.getWidth()/2, HEIGHT/2 - sprite.getHeight()/2);
    }
}
