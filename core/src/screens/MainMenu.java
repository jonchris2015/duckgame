package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;

import controllers.GameplayController;

/**
 * Created by C on 1/1/2017.
 */

public class MainMenu implements Screen, InputProcessor, MasterScreen{
    private SpriteBatch batch;
    private TextButton play;
    private TextButton quit;
    private TextButton help;
    private static final int WIDTH = 720;
    private static final int HEIGHT = 480;
    private Stage stage;
    private GameplayController controller;
    private BitmapFont gameTitle;
    private Skin mySkin;
    private float gameTitleX = WIDTH/2;
    private Sprite gameSign;

    public MainMenu(GameplayController controller, SpriteBatch batch){
        stage = new Stage();
        mySkin = new Skin(Gdx.files.internal("skin/clean-crispy-ui.json"));
        gameTitle = new BitmapFont();
        this.controller = controller;
        this.batch = batch;
        gameSign = new Sprite(controller.getTextures().get("Sign.png"));

        positionButtons();
        addListener();

        stage.addActor(play);
        stage.addActor(quit);
        stage.addActor(help);

        Gdx.input.setInputProcessor(stage);
    }

    public void setCurrentX(float x){
        gameTitleX = x;
    }

    public void positionButtons() {
        play = new TextButton("Play", mySkin);
        quit = new TextButton("Quit", mySkin);
        help = new TextButton("Help", mySkin);

        play.setSize(225, 50);
        quit.setSize(225, 50);
        help.setSize(225, 50);

        play.setPosition(WIDTH/2 - play.getWidth()/2, HEIGHT/2 + play.getHeight() + play.getHeight()/2);
        help.setPosition(WIDTH/2 - help.getWidth()/2, HEIGHT/2);
        quit.setPosition(WIDTH/2 - quit.getWidth()/2, HEIGHT/2 - (quit.getHeight()+ quit.getHeight()/2));

        gameSign.setPosition(WIDTH/2- gameSign.getWidth()/2f, HEIGHT / 2 + 2.7f*play.getHeight());
    }

    public void addListener() {
        play.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.startGame();
            }
        });

        quit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
//                System.exit(0);
            }
        });

        help.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.help(gameTitleX);
            }
        });
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glClearColor(.6f, .8f, 1f, 0f);

        batch.begin();
        gameSign.draw(batch);
        batch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        stage.dispose();
        gameTitle.dispose();
        mySkin.dispose();
        gameSign.getTexture().dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
    public Stage getStage(){
        return stage;
    }
}
