package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.HashMap;

import controllers.GameplayController;

/**
 * Created by C on 1/2/2017.
 */

public class HelpScreen implements Screen, InputProcessor, MasterScreen {
    private Stage stage;
    private ImageButton backBtn;
    private GameplayController controller;
    private Sprite bg;
    private SpriteBatch batch;
    private HashMap<String, Texture> textures;
    private float x;

    public HelpScreen(GameplayController controller, SpriteBatch batch){
        x = 360;
        this.controller = controller;
        textures = controller.getTextures();
        stage = new Stage();
        this.batch = batch;
        backBtn = new ImageButton(new SpriteDrawable(new Sprite(textures.get("Back.png"))));
        bg = new Sprite(textures.get("Help.png"));
        positionButtons();
        addListener();
        stage.addActor(backBtn);
    }

    @Override
    public void positionButtons() {
        backBtn.setPosition(backBtn.getWidth(), backBtn.getHeight());
    }

    @Override
    public void addListener() {
        backBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.mainMenuContinue(x);
            }
        });
    }
    
    public void setX(float x){
        this.x = x;
        bg.setPosition(x - bg.getWidth()/2, 240 - bg.getHeight()/2);
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glClearColor(.6f, .8f, 1f, 0f);

        batch.begin();
        bg.draw(batch);
        batch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        bg.getTexture().dispose();
        batch.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Stage getStage(){
        return stage;
    }

}
