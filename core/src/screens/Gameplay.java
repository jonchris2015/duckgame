package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import java.util.HashMap;
import collectables.Coin;
import collision.CoinDetection;
import collision.EnemyDetection;
import collision.PlatformDetection;
import controllers.GameplayController;
import helper.XBox360Controls;
import huds.HealthBar;
import mobs.Blob;
import mobs.Duck;
import platforms.Platform;

/**
 * Created by C on 12/31/2016.
 */

public class Gameplay implements Screen, InputProcessor, ControllerListener{
    private final Boolean DEBUG = true;
    private ShapeRenderer sr;
    private SpriteBatch batch;
    private Duck duck;
    private Blob blob;
    private Platform platform;
    private Platform platform2;
    private Platform platform3;
    private Platform platform4;
    private Platform platform5;
    private Sprite bgSprite;
    private Texture bg;
    private OrthographicCamera camera;
    private static final int WIDTH = 720;
    private static final int HEIGHT = 480;
    private boolean gamePaused = false;
    private Array<Integer> keysPressed;
    private ShapeRenderer shapeRenderer;
    private GameplayController controller;
    private HashMap<String, Texture> textures;
    private PlatformDetection platformDetection;
    private Array<Platform>platforms;
    private Array<String> collisions;
    private EnemyDetection enemyDetection;
    private HealthBar healthBar;
    private ShapeRenderer healthBarOut;
    private Coin coin;
    private CoinDetection coinDetection;

    public Gameplay(GameplayController controller, SpriteBatch batch){
        sr = new ShapeRenderer();

        textures = controller.getTextures();
        this.controller = controller;
        coin = new Coin(textures.get("Coin.png"));
        duck = new Duck(textures.get("Duck.png"));
        blob = new Blob(batch, controller);
        this.batch = batch;
        platform = new Platform(textures.get("Platform.png"), 300, 150);
        platform.createBody();
        platform2 = new Platform(textures.get("Platform.png"), 600, 100);
        platform2.createBody();
        platform3 = new Platform(textures.get("Platform.png"), 900, 0);
        platform3.createBody();
        platform4 = new Platform(textures.get("Platform.png"), 800, 360);
        platform4.createBody();
        platform5 = new Platform(textures.get("Platform.png"), 1350, 0);
        platform5.createBody();
        coin.setPosition(platform2.getX()+platform2.getWidth()/2, platform2.getY()+platform2.getHeight());
        duck.setPosition(WIDTH/2f, platform.getSprite().getY() + 150f);
        duck.setController(controller);
        blob.setPosition(platform2.getX()+platform2.getWidth()/2, platform2.getY()+platform2.getHeight()-6);
        blob.setPlatform(platform2);
        bg = textures.get("Background.png");
        enemyDetection = new EnemyDetection(batch, duck, blob, controller);
        collisions = new Array<>();
        platforms = new Array<>();
        platforms.addAll(platform, platform2, platform3, platform4, platform5);
        bgSprite = new Sprite(bg);
        camera = new OrthographicCamera(WIDTH, HEIGHT);
        camera.position.set(WIDTH/2, HEIGHT/2, 0);
        keysPressed = new Array<>();
        shapeRenderer = new ShapeRenderer();
        platformDetection = new PlatformDetection();
        platforms.addAll(platform, platform2, platform3);
        healthBarOut = new ShapeRenderer();
        healthBar = new HealthBar();
        healthBar.setSize(150, 30);
        healthBar.setOutlineWidth(150);
        duck.setHealthBar(healthBar);
        coinDetection = new CoinDetection();
    }

    public void fillMap(SpriteBatch batch){
        for(int i = -720; i > -7200; i-=720){
            bgSprite.setPosition(i, 0);
            bgSprite.draw(batch);
        }
        for(int i = 720; i < 1440; i+=720){
            bgSprite.setPosition(i, 0);
            bgSprite.draw(batch);
        }
    }

    public void darkenScreen(){
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        shapeRenderer.rect(0, 0, WIDTH, HEIGHT);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        shapeRenderer.setColor(new Color(0, 0, 0, 0.5f));
    }

    public void brightenScreen(){
        shapeRenderer.setColor(new Color(0, 0, 0, 0));
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        for(int i = 0; i < platforms.size; i++){
            platformDetection.setToCheck(duck.getParts(), platforms.get(i).getParts());
            if(platformDetection.collisionType().equals("landed")){

                if(duck.getYVelocity() < 0){
                    duck.setYVelocity(0);
                    collisions.add(platformDetection.collisionType());
                }
            }
            else {
                collisions.add(platformDetection.collisionType());
            }

        }

        duck.setCamera(camera);
        duck.setPaused(gamePaused);
        duck.setCollisions(collisions);
        duck.Update(delta);
        blob.Update(delta);
        healthBar.Update(duck);

        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glClearColor(.6f, .8f, 1f, 0f);
        bgSprite.setPosition(0, 0);

        duck.setEnemy(blob);

        blob.setPaused(gamePaused);

        boxStuff();

        batch.begin();
        bgSprite.draw(batch);
        fillMap(batch);

        if(coinDetection.checkCollision(duck, coin)){
            coin.setTaken(true);
        }

        if(!blob.getDead() && !coin.isTaken()) {
            coin.draw(batch);
        }
        blob.draw(batch);
        duck.draw(batch);

        if(blob.getDead() && !coin.isTaken()){
            coin.draw(batch);
        }

        enemyDetection.setTextures(textures);

        if(duck.getProjectile()!=null) {
            duck.getProjectile().draw(batch);
            duck.getProjectile().Update(delta, gamePaused);
            if(enemyDetection.detectProjectile(gamePaused)){
                blob.slowDown();
            }
        }

        if(enemyDetection.detectBody(gamePaused) == 0){
            controller.lose(duck.getSprite().getX());
        }

        for(int i = 0; i < platforms.size; i++){
            platforms.get(i).draw(batch);
        }
        batch.setProjectionMatrix(camera.combined);
        batch.end();

        boxStuff();

        healthBar.Update(duck);

        if(duck.getHealth() < 150) {
            if (duck.getHealRate() >= 1f) {
                duck.setHealth(duck.getHealth() + 1);
                duck.getHealthBar().reduceWidth(1);
                duck.setHealRate(0);
            } else {
                if(!gamePaused)
                duck.setHealRate(duck.getHealRate() + delta);
            }
        }

        camera.update();
        togglePause();
        checkBounds();
        collisions.clear();

    }

    private void boxStuff() {
        if(DEBUG) {
            sr.setAutoShapeType(true);
            sr.setProjectionMatrix(camera.combined);
            sr.begin(ShapeRenderer.ShapeType.Filled);
            healthBar.renderHealthBar(sr);
            sr.setColor(Color.RED);
            sr.end();

            healthBarOut.setAutoShapeType(true);
            healthBarOut.setProjectionMatrix(camera.combined);
            healthBarOut.begin();
//            blob.DebugDraw(healthBarOut);
//            duck.DebugDraw(healthBarOut);
            healthBar.renderOutline(healthBarOut);
            healthBarOut.setColor(Color.BLACK);
            healthBarOut.end();
        }
    }

    private void checkBounds() {
        if(duck.getSprite().getX() > 1440){
            controller.endOfLevel(duck.getSprite().getX());
        }
        if (duck.getSprite().getY() <= -duck.getSprite().getHeight()) {
            controller.lose(duck.getSprite().getX());
        }
    }

    private void togglePause() {

        if(!gamePaused) {
            brightenScreen();
        }

        else{
            darkenScreen();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        gamePaused = true;

        int j = 0;
        while(j < keysPressed.size){
            keyUp(keysPressed.get(0));
            j++;
        }

    }

    @Override
    public void resume() {
        gamePaused = false;
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        bg.dispose();
        duck.dispose();
        platform.dispose();
        platform2.dispose();
        platform3.dispose();
        shapeRenderer.dispose();
        blob.dispose();
        coin.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if(!gamePaused) {
            keysPressed.add(keycode);
            duck.KeyPressed(keycode);
            System.out.println(keycode);
        }

        if(keycode == Input.Keys.P && !gamePaused){
            pause();
        }

        else if (keycode == Input.Keys.P && gamePaused){
            resume();
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        keysPressed.removeValue(keycode, true);
        duck.KeyReleased(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void connected(Controller controller) {
        System.out.println("connected");
    }

    @Override
    public void disconnected(Controller controller) {
        System.out.println("disconnected");
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        switch (buttonCode) {
            case XBox360Controls.BUTTON_A:
                keyDown(51);
                break;
            case XBox360Controls.BUTTON_X:
                keyDown(62);
                break;
            case XBox360Controls.BUTTON_START:
                keyDown(44);
                break;
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        switch (buttonCode) {
            case XBox360Controls.BUTTON_A:
                keyUp(51);
                break;
            case XBox360Controls.BUTTON_X:
                keyUp(62);
                break;
            case XBox360Controls.BUTTON_START:
                keyUp(44);
                break;
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        if(value == XBox360Controls.BUTTON_DPAD_LEFT) {
            keyUp(32);
            keyDown(29);
        }
        if(value == XBox360Controls.BUTTON_DPAD_RIGHT){
            keyUp(29);
            keyDown(32);
        }
        if(value == PovDirection.center){
            keyUp(29);
            keyUp(32);
        }
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }
}
