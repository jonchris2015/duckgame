package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import java.util.HashMap;

import controllers.GameplayController;
import helper.GameInfo;

/**
 * Created by C on 1/4/2017.
 */

public class LoseScreen implements MasterScreen, InputProcessor, Screen {
    private Stage stage;
    private SpriteBatch batch;
    private Sprite duckLose;
    private GameplayController controller;
    private ImageButton replayButton;
    private ImageButton mainMenuButton;
    private HashMap<String, Texture> textures;
    private float duckLosePos;

    public LoseScreen(GameplayController controller, SpriteBatch batch){
        stage = new Stage();
        this.controller = controller;
        this.batch = batch;
        textures = controller.getTextures();
        duckLose = new Sprite(textures.get("LoseDuck.png"));
        replayButton = new ImageButton(new SpriteDrawable(new Sprite(textures.get("Replay.png"))));
        mainMenuButton = new ImageButton(new SpriteDrawable(new Sprite(textures.get("MainMenu.png"))));

        positionButtons();
        addListener();

        stage.addActor(replayButton);
        stage.addActor(mainMenuButton);
    }

    @Override
    public void positionButtons() {
        replayButton.setPosition(GameInfo.WIDTH/2 - replayButton.getWidth(), GameInfo.HEIGHT/4);
        mainMenuButton.setPosition(GameInfo.WIDTH/2, GameInfo.HEIGHT/4);
    }

    @Override
    public void addListener() {
        replayButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.startGame();
            }
        });
        mainMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                controller.mainMenuContinue(duckLosePos);
            }
        });
    }

    public void setDuckDrawing(float duckDrawingPosX) {
        duckLosePos = duckDrawingPosX;
        System.out.println(duckLosePos);
        duckLose.setPosition(duckLosePos - duckLose.getWidth()/2, GameInfo.HEIGHT/2);
    }

    public Stage getStage(){
        return stage;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glClearColor(0f, 0f, 0f, 0f);

        batch.begin();
        duckLose.draw(batch);
        batch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        duckLose.getTexture().dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
