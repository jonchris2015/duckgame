package collectables;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by C on 1/14/2017.
 */

public class Coin {
    private Sprite sprite;
    private boolean taken = false;

    public Coin(Texture texture){
        sprite = new Sprite(texture);
    }

    public void setPosition(float x, float y){
        sprite.setPosition(x, y);
    }

    public boolean isTaken(){
        return taken;
    }

    public void setTaken(boolean taken){
        this.taken = taken;
    }

    public Rectangle getHitbox() {
        return sprite.getBoundingRectangle();
    }

    public void draw(SpriteBatch batch){
        sprite.draw(batch);
    }

    public void dispose(){
        if(sprite.getTexture()!=null)
        sprite.getTexture().dispose();
    }
}
