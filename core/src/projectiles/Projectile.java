package projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by C on 1/6/2017.
 */
public class Projectile {
    public Sprite sprite;
    public Vector2 velocity;
    public boolean left = false;
    public int damage;

    public Projectile(Texture texture, float x, float y){
        sprite = new Sprite(texture);
        sprite.setPosition(x, y);
        velocity = new Vector2(2000, 0);
        damage = 1;
    }

    public void Update(float delta, boolean paused){
        sprite.setX(sprite.getX() + velocity.x * delta);
        sprite.setY(sprite.getY() + velocity.y * delta);

    }

    public void setDirection(boolean left){
        this.left = left;
    }

    public void draw(SpriteBatch batch){
        sprite.draw(batch);
    }

    public Rectangle getHitbox(){
        return sprite.getBoundingRectangle();
    }

    public boolean checkOverlap(Rectangle rectangle){
        boolean overlap = getHitbox().overlaps(rectangle);
        return overlap;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void dispose(){
        getSprite().getTexture().dispose();
    }

    public int getDamage(){
        return damage;
    }
}
