package projectiles;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;

import java.util.HashMap;

import controllers.GameplayController;

/**
 * Created by C on 1/6/2017.
 */

public class WaterBlast extends Projectile{
    private Game controller;
    private HashMap<String, Texture> textures;

    public WaterBlast(Texture texture, float x, float y) {
        super(texture, x, y);
        damage = 7;
    }

    public void setController(Game controller){
        this.controller = controller;

        if(controller instanceof GameplayController){
            textures = ((GameplayController) controller).getTextures();
        }
    }

    public void Update(float delta, boolean paused){
        if(!paused) {
            if(!left) {
                if(velocity.x > 150f) {
                    velocity.x -= 150f;
                }
                else if (velocity.x <= 150f){
                    velocity.x = 0;
                    sprite.setTexture(textures.get("Blank.png"));
                }

                sprite.setX(sprite.getX() + velocity.x * delta);
                sprite.setY(sprite.getY() + velocity.y * delta);

            }
            else{
                if(velocity.x > 150f){
                    velocity.x -=150f;
                }
                else if (velocity.x <= 150f){
                    velocity.x = 0;
                    sprite.setTexture(textures.get("Blank.png"));
                }
                sprite.setX(sprite.getX() + (-velocity.x * delta));
                sprite.setY(sprite.getY() + (-velocity.y * delta));
            }
        }
    }
}
