package jumps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

import mobs.Duck;

/**
 * Created by C on 1/6/2017.
 */

public class JumpBase {
    private int counter = 0;
    private float heightLeftToJump = 0f;
    private float maxHeightToJump = 0f;
    private int maxJumps = 1;
    private float currentGravity;
    private float gravityPerSecond = 100;
    private Duck duck;
    float heightToFall = 0;

    public JumpBase(int jumps){
        this.maxJumps = jumps;
    }

    public void Update(float delta, Duck duck, Array<String> collisions){
        if(currentGravity > 200){
            currentGravity = 200;
        }
        this.duck = duck;
        maxHeightToJump = duck.getHeight()*1.5f;

        if(duck.getAirBourne() && !duck.getPause()){
            currentGravity += gravityPerSecond * delta;
            heightToFall = currentGravity;
        }

            if(currentGravity > heightLeftToJump || collisions.contains("under", true) ||
                    duck.getY() > Gdx.graphics.getHeight()){
                heightLeftToJump = 0;
            }

        float yVelocity = (heightLeftToJump - heightToFall);
        duck.setYVelocity(yVelocity);
    }
    public void jumpPressed(){
        if(counter < maxJumps){
            currentGravity = 0;
            counter++;
            heightLeftToJump = maxHeightToJump;
            duck.setYVelocity(0);
        }
    }

    public void jumpReleased(){


    }

    public void jumpLanded(){
        if(heightLeftToJump <= 0) {
            heightLeftToJump = 0;
        }
        counter = 0;
        currentGravity = 0;

    }
}
