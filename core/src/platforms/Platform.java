package platforms;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by C on 1/1/2017.
 */

public class Platform {
    private Sprite sprite;
    private Texture texture;
    private Rectangle top;
    private Rectangle middle;
    private Rectangle bottom;
    private Rectangle[] parts;

    public Platform(Texture texture, float x, float y){
        this.texture = texture;
        sprite = new Sprite(texture);
        sprite.setPosition(x, y);

        top = new Rectangle();
        middle = new Rectangle();
        bottom = new Rectangle();

        parts = new Rectangle[3];
        createBody();
    }

    public float getX(){
        return sprite.getX();
    }

    public float getY(){
        return sprite.getY();
    }

    public void createBody() {
        top.setSize(sprite.getWidth(), sprite.getHeight()*(.10f));
        middle.setSize(sprite.getWidth(), sprite.getHeight()*(.70f));
        bottom.setSize(sprite.getWidth(), sprite.getHeight()*(.20f));

        top.setPosition(sprite.getX(), sprite.getY()+(sprite.getHeight()*.90f));
        middle.setPosition(sprite.getX(), sprite.getY()+(sprite.getHeight()*.10f));
        bottom.setPosition(sprite.getX(), sprite.getY());

        parts[0] = top;
        parts[1] = middle;
        parts[2] = bottom;
    }

    public Rectangle[] getParts(){
        return parts;
    }

    public float getHeight(){
        return sprite.getHeight();
    }

    public float getWidth(){
        return sprite.getWidth();
    }

    public Sprite getSprite(){ return sprite;}

    public void draw(SpriteBatch batch){
        sprite.draw(batch);
    }

    public void dispose(){
        texture.dispose();
    }
}
