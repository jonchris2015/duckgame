package singleton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

//import java.io.File;
//import java.nio.file.Path;
//import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by C on 1/3/2017.
 */
public class TextureSingleton {
    private static HashMap<String, Texture> ourInstance = null;
    private static final String fileLocation = "images/";

    public static HashMap<String, Texture> getSingleton() {
        if(ourInstance == null){
            ourInstance = new HashMap<>();
                    ourInstance.put("Back.png", new Texture(fileLocation + "Back.png"));
                    ourInstance.put("Background.png", new Texture(fileLocation + "Background.png"));
                    ourInstance.put("Blank.png", new Texture(fileLocation + "Blank.png"));
                    ourInstance.put("Blob.png", new Texture(fileLocation + "Blob.png"));
                    ourInstance.put("BlobDead.png", new Texture(fileLocation + "BlobDead.png"));
                    ourInstance.put("Duck.png", new Texture(fileLocation + "Duck.png"));
                    ourInstance.put("DuckWin.png", new Texture(fileLocation + "DuckWin.png"));
                    ourInstance.put("Game Icon.png", new Texture(fileLocation + "Game Icon.png"));
                    ourInstance.put("GameTitle.png", new Texture(fileLocation + "GameTitle.png"));
                    ourInstance.put("Help.png", new Texture(fileLocation + "Help.png"));
                    ourInstance.put("LoseDuck.png", new Texture(fileLocation + "LoseDuck.png"));
                    ourInstance.put("MainMenu.png", new Texture(fileLocation + "MainMenu.png"));
                    ourInstance.put("Next.png", new Texture(fileLocation + "Next.png"));
                    ourInstance.put("Platform.png", new Texture(fileLocation + "Platform.png"));
                    ourInstance.put("Replay.png", new Texture(fileLocation + "Replay.png"));
                    ourInstance.put("WaterBlast.png", new Texture(fileLocation + "WaterBlast.png"));
                    ourInstance.put("Coin.png", new Texture(fileLocation + "Coin.png"));
                    ourInstance.put("Sign.png", new Texture(fileLocation + "Sign.png"));
        }
        return ourInstance;
    }

}
