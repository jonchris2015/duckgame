package mobs;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

import controllers.GameplayController;
import platforms.Platform;

/**
 * Created by C on 1/11/2017.
 */

public class Blob implements Mob{
    private Sprite sprite;
    private SpriteBatch batch;
    private HashMap<String, Texture> textures;
    private Rectangle head;
    private Rectangle body;
    private int health;
    private boolean deceased;
    private Rectangle[] parts;
    private Vector2 velocity;
    private float YVelocity;
    private Platform platform;
    private boolean paused = false;
    private boolean flipped = false;
    private float coolDown = 0f;

    public Blob(SpriteBatch batch, GameplayController controller){
        this.batch = batch;
        textures = controller.getTextures();
        sprite = new Sprite(textures.get("Blob.png"));
        health = 150;
        deceased = false;
        parts = new Rectangle[2];
        velocity = new Vector2(500, 0);

        createBody();
    }

    public void setPlatform(Platform platform){
        this.platform = platform;
    }

    @Override
    public void Update(float delta) {
        if(!getDead() && !paused) {
            if(!flipped) {
                if (sprite.getX() + sprite.getWidth() < platform.getX() + platform.getWidth()) {
                    sprite.setX(sprite.getX() + velocity.x*delta);
                } else if (sprite.getX() + sprite.getWidth() >= platform.getX() + platform.getWidth()) {
                    sprite.flip(true, false);
                    flipped = true;
                }
            }

            if(flipped) {
                if (sprite.getX() > platform.getX() && sprite.isFlipX()) {
                    sprite.setX(sprite.getX() - velocity.x*delta);
                } else if (sprite.getX() <= platform.getX() && sprite.isFlipX()) {
                    sprite.flip(true, false);
                    flipped = false;
                }
            }
            rectangleHitboxes();
        }
        coolDown+=delta;
        if(coolDown>=.33f){
            velocity.x = 500;
        }
    }

    @Override
    public void setCamera(OrthographicCamera camera) {

    }

    @Override
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    @Override
    public void setCollisions(Array<String> collisions) {

    }

    @Override
    public void draw(SpriteBatch batch) {
        sprite.draw(batch);
    }

    @Override
    public void createBody() {
        head = new Rectangle();
        body = new Rectangle();
    }

    @Override
    public void setPosition(float x, float y) {
        this.sprite.setPosition(x, y);
        rectangleHitboxes();
    }

    public void slowDown(){
        if(velocity.x > 250) {
            velocity.x /= 3;
            coolDown = 0;
        }
    }

    private void rectangleHitboxes() {
        head.setSize(sprite.getWidth()*(2/3f), sprite.getHeight() * (2/10f));
        body.setSize(sprite.getWidth(), sprite.getHeight() * (8/10f));

        if(flipped) {
            head.setPosition(sprite.getX(), sprite.getY() + (sprite.getHeight() * 8 / 10));
        }
        else{
            head.setPosition(sprite.getX() + (sprite.getWidth() * 1 / 3),sprite.getY() + (sprite.getHeight() * 8 / 10));
        }
        body.setPosition(sprite.getX(), sprite.getY());

        parts[0] = head;
        parts[1] = body;
    }

    public Rectangle getHead(){
        return head;
    }

    public Rectangle getBody(){
        return body;
    }

    @Override
    public void dispose() {
        sprite.getTexture().dispose();
    }

    public void setDead(boolean state){
        this.deceased = state;
    }

    public void DebugDraw(ShapeRenderer sr){
        for(Rectangle collision : getParts()){
            sr.rect(collision.x, collision.y, collision.getWidth(), collision.getHeight());
        }
    }

    public Rectangle[] getParts(){
        return parts;
    }

    public boolean getDead(){
        return deceased;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int newHealth){
        health = newHealth;
    }

    public void setTexture(Texture texture){
        sprite.setTexture(texture);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setYVelocity(int YVelocity) {
        this.YVelocity = YVelocity;
    }
}
