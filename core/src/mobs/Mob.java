package mobs;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * Created by C on 1/1/2017.
 */

public interface Mob {
    public void Update(float delta);
    public void setCamera(OrthographicCamera camera);
    public void setPaused(boolean paused);
    public void setCollisions(Array<String>collisions);
    public void draw(SpriteBatch batch);
    public void createBody();
    public void setPosition(float x, float y);
    public void dispose();
}
