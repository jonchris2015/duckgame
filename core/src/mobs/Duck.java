package mobs;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

import controllers.GameplayController;
import huds.HealthBar;
import jumps.JumpBase;
import projectiles.WaterBlast;

/**
 * Created by C on 12/31/2016.
 */

public class Duck implements Mob{
    private SpriteBatch batch;
    private Sprite sprite;
    private Vector2 velocity;
    private boolean leftKey = false;
    private boolean rightKey = false;
    private boolean space = false;
    private OrthographicCamera camera;
    private Rectangle head;
    private Rectangle body;
    private Rectangle feet;
    private Rectangle[] parts;
    private boolean airBourne = false;
    private JumpBase jump;
    private boolean pause = false;
    private GameplayController controller;
    private WaterBlast waterBlast;
    private HashMap<String, Texture> textures;
    private float lastFired = 1f;
    private float coolDownTime = 1f;
    private boolean left = false;
    private Mob enemy;
    private int health;
    private Array<String> collisions;
    private HealthBar healthBar;
    private float healRate = 0;

    public Duck(Texture texture){
        jump = new JumpBase(5);
        sprite = new Sprite(texture);
        velocity = new Vector2(150, 0);
        head = new Rectangle();
        body = new Rectangle();
        feet = new Rectangle();

        parts = new Rectangle[3];

        health = 150;

        collisions = new Array<String>();

        createBody();
    }

    public void setController(GameplayController controller){
        this.controller = controller;
        textures = controller.getTextures();
    }

    public void createBody() {
        head.setWidth(sprite.getWidth());
        body.setWidth(sprite.getWidth());
        feet.setWidth(sprite.getWidth());

        head.setHeight(sprite.getHeight()*(.4f));
        body.setHeight(sprite.getHeight()*(.4f));
        feet.setHeight(sprite.getHeight()*(.2f));

        rectangleHitboxes();
    }

    public void setYVelocity(float yVelocity){
        this.velocity.y = yVelocity;
    }

    public void Update(float delta){

        if(leftKey){
            left = true;
            if(!sprite.isFlipX()){
                sprite.flip(true, false);
            }
            if(this.sprite.getX() > 0 && !collisions.contains("left", true)){
                sprite.setX(sprite.getX() - velocity.x * delta);
                camera.position.x -= delta * velocity.x;
            }
        }
        if (rightKey){
            left = false;
            if(sprite.isFlipX()){
                sprite.flip(true, false);
            }
            if(!collisions.contains("right", true)) {
                sprite.setX(sprite.getX() + velocity.x * delta);
                camera.position.x += delta * velocity.x;
            }
        }

        if(space){
            if(lastFired > coolDownTime) {
                waterBlast = new WaterBlast(textures.get("WaterBlast.png"),
                        getX() + getWidth(), parts[0].getY() - parts[0].getHeight());
                waterBlast.setController(controller);
                if(left){
                    waterBlast.getSprite().flip(true, false);
                    waterBlast.getSprite().setPosition(getX() - getWidth()/2,
                            parts[0].getY() - parts[0].getHeight());
                }
                waterBlast.setDirection(left);
                lastFired = 0;
            }
        }
        jump.Update(delta, this, collisions);

        if(!pause && !collisions.contains("landed", true)) {
            sprite.setY(sprite.getY() + velocity.y * delta);
            airBourne = true;
        }
        else if(!pause && collisions.contains("landed", true)){
            jump.jumpLanded();
            airBourne = false;
        }

        rectangleHitboxes();

        lastFired += delta;
    }

    public void setPaused(boolean pause){
        this.pause = pause;
    }

    public void setCollisions(Array<String> collisions){
        this.collisions = collisions;
    }

    public void setCamera(OrthographicCamera camera){
        this.camera = camera;
    }

    public WaterBlast getProjectile() {
        return waterBlast;
    }

    public float getY(){
        return this.sprite.getY();
    }

    public float getX(){
        return this.sprite.getX();
    }

    public void draw(SpriteBatch batch){
        this.batch = batch;
        sprite.draw(batch);
    }

    public void DebugDraw(ShapeRenderer sr){
        for(Rectangle collision : getParts()){
            sr.rect(collision.x, collision.y, collision.getWidth(), collision.getHeight());
        }
    }

    public Sprite getSprite(){
        return sprite;
    }

    public void setPosition(float x, float y){
        this.sprite.setPosition(x, y);
        rectangleHitboxes();
    }

    public void rectangleHitboxes() {
        head.setPosition(sprite.getX(), sprite.getY()+(sprite.getHeight()*(.6f)));
        body.setPosition(sprite.getX(), sprite.getY()+(sprite.getHeight()*(.2f)));
        feet.setPosition(sprite.getX(), sprite.getY());

        parts[0] = head;
        parts[1] = body;
        parts[2] = feet;
    }

    public void KeyPressed(int keycode){
        boolean pressed = true;
        switch(keycode){
            case Keys.A:
                leftKey = pressed;
                break;
            case Keys.D:
                rightKey = pressed;
                break;
            case Keys.W:
                jump.jumpPressed();
                break;
            case Keys.SPACE:
                space = pressed;
                break;
        }
    }

    public void KeyReleased(int keycode){
        boolean pressed = false;
        switch(keycode){
            case Keys.A:
                leftKey = pressed;
                break;
            case Keys.D:
                rightKey = pressed;
                break;
            case Keys.W:
                jump.jumpReleased();
                break;
            case Keys.SPACE:
                space = pressed;
                break;

        }
    }

    public void dispose(){
        sprite.getTexture().dispose();
        if(waterBlast!=null) {
            waterBlast.dispose();
        }
    }

    public int getHealth(){
        return health;
    }

    public void setHealth(int newHealth){
        health = newHealth;
    }

    public Rectangle[] getParts(){
        return parts;
    }

    public boolean getAirBourne(){
        return airBourne;
    }

    public boolean getPause() {
        return pause;
    }

    public float getYVelocity(){
        return velocity.y;
    }
    public float getHeight(){
        return sprite.getHeight();
    }

    public float getWidth() {
        return sprite.getWidth();
    }

    public void setEnemy(Blob enemy) {
        this.enemy = enemy;
    }

    public HealthBar getHealthBar() {
        return healthBar;
    }

    public void setHealthBar(HealthBar healthBar) {
        this.healthBar = healthBar;
    }

    public void setHealRate(float healRate) {
        this.healRate = healRate;
    }
    public float getHealRate(){
        return healRate;
    }
}
