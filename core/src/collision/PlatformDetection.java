package collision;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by C on 1/5/2017.
 */

public class PlatformDetection {
    private Rectangle[] player;
    private Rectangle[] platform;

    public PlatformDetection() {

    }

    public void setToCheck(Rectangle[]player, Rectangle[] platform){
        this.player = player;
        this.platform = platform;
    }

    public String collisionType() {
        if (player[2].getY() <= platform[0].getY() + platform[0].getHeight()
                && player[2].getY() > (platform[0].getY())
                && player[2].getX() + player[2].getWidth() > platform[0].getX()
                && player[2].getX() < platform[0].getX() + platform[0].getWidth()) {
            return "landed";
        }

        if (player[0].getY()+player[0].getHeight() >= platform[2].getY()
                && player[0].getY()+player[0].getHeight() < platform[2].getY() + platform[2].getHeight()
                && player[0].getX() + player[0].getWidth() > platform[2].getX()
                && player[0].getX() < platform[2].getX() + platform[2].getWidth()
                ){
            return "under";
        }

        /*
            Checks side of platform collisions
         */
        if(player[0].getY()+player[0].getHeight() >= platform[1].getY()
                && player[2].getY() < platform[1].getY()+platform[1].getHeight()){

            if (player[0].getX() <= platform[1].getX() + platform[1].getWidth()
                    && player[0].getX() > platform[1].getX()) {
                return "left";
            }
            if ((player[0].getX() + player[0].getWidth() >= platform[1].getX()
                && player[0].getX()+player[0].getWidth() < platform[1].getX()+platform[1].getWidth())){
                    return "right";
                }
        }
        return "";
    }
}
