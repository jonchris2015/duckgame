package collision;

import collectables.Coin;
import mobs.Duck;

/**
 * Created by C on 1/15/2017.
 */

public class CoinDetection {
    private Duck duck;
    private Coin coin;

    public CoinDetection(){
    }

    public boolean checkCollision(Duck duck, Coin coin){
        return duck.getSprite().getBoundingRectangle().overlaps(coin.getHitbox());
    }
}
