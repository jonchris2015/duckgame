package collision;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import controllers.GameplayController;
import mobs.Blob;
import mobs.Duck;
import mobs.Mob;

/**
 * Created by C on 1/13/2017.
 */

public class EnemyDetection {
    private SpriteBatch batch;
    private Duck duck;
    private Mob blob;
    private HashMap<String, Texture> textures;

    public EnemyDetection(SpriteBatch batch, Duck duck, Mob mob, GameplayController controller){
        this.batch = batch;
        this.duck = duck;
        blob = mob;
    }

    public void setTextures(HashMap<String, Texture>textures){
        this.textures = textures;
    }

    public int detectBody(boolean gamePaused){
        if(duck.getParts()[2].overlaps(((Blob)blob).getHead()) && !((Blob)blob).getDead()&&
                duck.getYVelocity()<0){
            ((Blob)blob).setHealth(0);
            ((Blob)blob).setTexture(textures.get("BlobDead.png"));
            ((Blob)blob).setDead(true);
        }
        if(duck.getSprite().getBoundingRectangle().overlaps(((Blob)blob).getBody()) && !((Blob)blob).getDead() && !gamePaused) {
            if (duck.getHealth() > 0) {
                duck.setHealth(duck.getHealth() - 1);
                duck.getHealthBar().reduceWidth(-1);
                duck.setHealRate(0);
                return -1;
            }
        }
        if(duck.getHealth() <= 0){
            return 0;
        }
        return duck.getHealth();
    }

    public boolean detectProjectile(boolean gamePaused){
        if(duck.getProjectile().checkOverlap(((Blob)blob).getHead()) && duck.getProjectile().velocity.x >0 && !gamePaused
                && !((Blob) blob).getDead()){
            ((Blob)blob).setHealth(((Blob)blob).getHealth()-duck.getProjectile().getDamage());
            if(((Blob) blob).getHealth()>0){
                return true;
            }
            else{
                ((Blob)blob).setTexture(textures.get("BlobDead.png"));
                ((Blob)blob).setHealth(0);
                ((Blob)blob).setDead(true);
                return false;
            }
        }
        return false;
    }
}


